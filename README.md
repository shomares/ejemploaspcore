# Ejemploaspcore
Ejemplo de una api utilizando asp.net core 2 con base de datos postgres en linux (Fedora). En contenedores docker.

⋅⋅*[Instalar docker](https://docs.docker.com/install/linux/docker-ce/fedora/#install-docker-ce-1) 
⋅⋅*[Instalar docker-composer](https://github.com/NaturalHistoryMuseum/scratchpads2/wiki/Install-Docker-and-Docker-Compose-(Centos-7)) 

Pasos:
1. Ejecutar `cd/dulceria`
2. Compilar el docker composer `docker-compose build`
3. Para correrlo `docker-compose up`
4. Actualizar la base de datos: `dotnet ef database update`, dentro de la carpeta backend.


Eneas Baltazar Mejia Shomar en casa de QUMA
2019 shomares@gmail.com
