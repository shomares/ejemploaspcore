using System.Collections.Generic;
using System.Threading.Tasks;

public interface IBussinesVenta
{
    Task<MVenta> Vender(MVenta vender);

    Task<IEnumerable<MProducto>> GetAll();

    Task<IEnumerable<MVenta>> GetVentasByProducto(int id);

    Task<MProducto> Suministrar(MProducto producto);

    Task<IEnumerable<MBitacoraProducto>> ConsultarBitacora(MBitacoraParamatero parametro);
    
}