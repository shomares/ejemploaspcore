using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

public class BussinesVenta : IBussinesVenta
{
    private readonly IDaoProducto daoProducto;
    private readonly IDaoVenta daoVenta;

    private readonly IDaoListaVenta daoListaVenta;

    private readonly IDaoBitacoraProducto daoBitacoraProducto;

    public BussinesVenta(IDaoProducto daoProducto, IDaoVenta daoVenta, IDaoListaVenta daoListaVenta, IDaoBitacoraProducto daoBitacoraProducto)
    {
        this.daoProducto= daoProducto;
        this.daoVenta= daoVenta;
        this.daoListaVenta= daoListaVenta;
        this.daoBitacoraProducto= daoBitacoraProducto;
    }

    public async Task<IEnumerable<MBitacoraProducto>> ConsultarBitacora(MBitacoraParamatero parametro)
    {
        return await this.daoBitacoraProducto.Consultar(parametro);
    }

    public async Task<IEnumerable<MProducto>> GetAll()
    {
        var producto =  await this.daoProducto.GetAll();

        return producto.Select(x=>new MProducto{
            Cantidad= x.Cantidad,
            Fecha= x.Fecha,
            Nombre= x.Nombre,
            ProductoId= x.ProductoId
        }).ToArray();
    }

    public async Task<IEnumerable<MVenta>> GetVentasByProducto(int id)
    {
        return await this.daoProducto.GetVentasByProducto(id);
    }

    public async Task<MProducto> Suministrar(MProducto producto)
    {
        Producto prodDB= null;
        if(producto.ProductoId==null)
        {
            prodDB= new Producto{
                Nombre= producto.Nombre,
                Cantidad = producto.Cantidad,
                Fecha = DateTime.Now
            };

            var s = await this.daoProducto.Add(prodDB);
            prodDB.ProductoId = s;

        }else
        {
            prodDB = await this.daoProducto.GetById(producto.ProductoId.Value);
            if(prodDB!=null)
            {
                prodDB.Cantidad = producto.Cantidad + prodDB.Cantidad;
                prodDB = await this.daoProducto.Update(prodDB);
            }
        }

        if(prodDB!=null)
        {
            return new MProducto{
                    ProductoId= prodDB.ProductoId
            };
        }
        else
        {
            return null;
        }
    }

    public async Task<MVenta> Vender(MVenta vender)
    {
        int correct= 0;
        Venta venta = new Venta{
            Fecha = DateTime.Now
        };

        venta.VentaId = await this.daoVenta.Add(venta);

        foreach(var elemento in vender.Ventas)
        {
            var producto = await this.daoProducto.GetById(elemento.ProductoId);

            if(producto!=null)
            {
                producto.Cantidad -= elemento.Cantidad;
                if(producto.Cantidad>0)
                {
                    await this.daoProducto.Update(producto);
                    await this.daoListaVenta.Add(new ListaVenta{
                           ProductoId= producto.ProductoId,
                           VentaId = venta.VentaId,
                           Cantidad = elemento.Cantidad 
                    });

                    correct++;
                }
            }
        }

        if(correct==0)
        {
            throw new Exception();
        }

        return new MVenta{
            IdVenta= venta.VentaId
        };
    }
}