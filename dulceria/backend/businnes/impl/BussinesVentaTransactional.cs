using System.Collections.Generic;
using System.Threading.Tasks;
using System;
public class BussinesVentaTransactional : IBussinesVenta
{
    private readonly IBussinesVenta bussines;
    private readonly ITransactional transactional;

    public BussinesVentaTransactional(IBussinesVenta bussines,ITransactional transactional )
    {
        this.bussines= bussines;
        this.transactional= transactional;
    }

    public async Task<IEnumerable<MBitacoraProducto>> ConsultarBitacora(MBitacoraParamatero parametro)
    {
              try
        {
            this.transactional.Begin();
            var res=  await this.bussines.ConsultarBitacora(parametro);
            this.transactional.Commit();
            return res;
        }catch(Exception)
        {
            this.transactional.Rollback();
            throw;
        }
        finally
        {
            this.transactional.Dispose();
        }
    }

    public async Task<IEnumerable<MProducto>> GetAll()
    {
        try
        {
            this.transactional.Begin();
            var res=  await this.bussines.GetAll();
            this.transactional.Commit();
            return res;
        }catch(Exception)
        {
            this.transactional.Rollback();
            throw;
        }
        finally
        {
            this.transactional.Dispose();
        }
    }

    public async Task<IEnumerable<MVenta>> GetVentasByProducto(int id)
    {
       try
        {
            this.transactional.Begin();
            var res=  await this.bussines.GetVentasByProducto(id);
            this.transactional.Commit();
            return res;
        }catch(Exception)
        {
            this.transactional.Rollback();
            throw;
        }
        finally
        {
            this.transactional.Dispose();
        }
    }

    public async Task<MProducto> Suministrar(MProducto producto)
    {
       try
        {
            this.transactional.Begin();
            var res=  await this.bussines.Suministrar(producto);
            this.transactional.Commit();
            return res;
        }catch(Exception)
        {
            this.transactional.Rollback();
            throw;
        }
        finally
        {
            this.transactional.Dispose();
        }
    }

    public async Task<MVenta> Vender(MVenta vender)
    {
         try
        {
            this.transactional.Begin();
            var res=  await this.bussines.Vender(vender);
            this.transactional.Commit();
            return res;
        }catch(Exception)
        {
            this.transactional.Rollback();
            throw;
        }
        finally
        {
            this.transactional.Dispose();
        }
    }
}