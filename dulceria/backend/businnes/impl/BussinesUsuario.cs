using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class BussinesUsuario : IBussinessUsuario
{
    private readonly IDaoUsuario iDaoUsuario;
    private readonly IHashService iHashService;

    public BussinesUsuario(IDaoUsuario iDaoUsuario, IHashService iHashService)
    {
        this.iDaoUsuario = iDaoUsuario;
        this.iHashService= iHashService;
    }

    public async Task<MUsuario> Authenticate(string username, string password)
    {
        string hash= string.Empty;
        var user = await this.iDaoUsuario.GetByUsername(username);
        if(user!=null)
        {
            hash = this.iHashService.GetHash(password, user.Salt);
            if(hash.Equals(user.Password) && user.Habilitado)
            {
                var s=  new MUsuario{
                        Username= username,
                        ApellidoPaterno= user.ApellidoPaterno,
                        ApelludoMaterno= user.ApelludoMaterno
                };

                var token = this.iHashService.GetToken(s);
                s.Token= token;
                return s;
            }
            
            return null;

        }

        return null;
    }

    public async Task<bool> Borrar(int id)
    {
        var user = await this.iDaoUsuario.GetById(id);

        if(user!=null)
        {
            user.Habilitado= false;
            await this.iDaoUsuario.Update(user);
        }

        return user!=null;
    }

    public async Task<IEnumerable<MUsuario>> ConsultarTodos()
    {
        var res = await this.iDaoUsuario.GetAll();
        if(res!=null)
        {
            return res.Select(s=>new MUsuario{
                ApellidoPaterno= s.ApellidoPaterno,
                ApelludoMaterno= s.ApelludoMaterno,
                Nombre= s.Nombre,
                Habilitado = s.Habilitado
            }).ToArray();
        }

        return null;
    }

    public async Task<MUsuario> Editar(MUsuario usuario)
    {
        var res = await this.iDaoUsuario.Update(new Usuario{
            ApellidoPaterno= usuario.ApellidoPaterno,
            ApelludoMaterno= usuario.ApelludoMaterno,
            Id= usuario.Id,
            Habilitado= usuario.Habilitado
        });

        if(res!=null)
        {
            return new MUsuario{
              Id= res.Id  
            };
        }

        return null;
    }

    public async Task<MUsuario> GetById(int id)
    {
        var res = await this.iDaoUsuario.GetById(id);

        if(res!=null)
        {
            return new MUsuario{
                ApellidoPaterno= res.ApellidoPaterno,
                ApelludoMaterno= res.ApelludoMaterno,
                Habilitado= res.Habilitado,
                Id= res.Id,
                Nombre= res.Nombre,
                Username= res.Username
            };
        }

        return null;
    }

    public async Task<MUsuario> Registrar(MUsuario usuario)
    {
        var salt = this.iHashService.GetSalt();

        var password = this.iHashService.GetHash(usuario.Password, salt);

        var res = await this.iDaoUsuario.Add(new Usuario{
            Salt= salt,
            ApellidoPaterno= usuario.ApellidoPaterno,
            ApelludoMaterno= usuario.ApelludoMaterno,
            Habilitado= true,
            Nombre= usuario.Nombre,
            Password= password,
            Username= usuario.Username,
        });

        if(res!=0)
        {
            return new MUsuario{
                Id= res
            };
        }

        return null;
    }
}