using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class BussinesUsuarioTransactional : IBussinessUsuario
{
    private readonly ITransactional iTransactional;
    private readonly IBussinessUsuario iBussinessUsuario;

    public BussinesUsuarioTransactional(ITransactional iTransactional, IBussinessUsuario iBussinessUsuario)
    {
        this.iTransactional= iTransactional;
        this.iBussinessUsuario= iBussinessUsuario;
    }
    public async Task<MUsuario> Authenticate(string username, string password)
    {
        try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.Authenticate(username, password);
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    
    }

    public async Task<bool> Borrar(int id)
    {
        try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.Borrar(id);
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    }

    public async Task<IEnumerable<MUsuario>> ConsultarTodos()
    {
        try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.ConsultarTodos();
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    }

    public async Task<MUsuario> Editar(MUsuario usuario)
    {
        try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.Editar(usuario);
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    }

    public async Task<MUsuario> GetById(int id)
    {
         try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.GetById(id);
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    }

    public async Task<MUsuario> Registrar(MUsuario usuario)
    {
        try
        {
            this.iTransactional.Begin();
            var res = await this.iBussinessUsuario.Registrar(usuario);
            this.iTransactional.Commit();
            return res;

        }catch(Exception)
        {
            this.iTransactional.Rollback();
            throw;

        }finally
        {
            this.iTransactional.Dispose();
        }
    }
}