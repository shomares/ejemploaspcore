using System.Collections.Generic;
using System.Threading.Tasks;

public interface IBussinessUsuario
{
    Task<MUsuario> Authenticate (string username, string password);

    Task<MUsuario> Registrar(MUsuario usuario);

    Task<bool> Borrar (int id);

    Task<MUsuario> Editar(MUsuario usuario);

    Task<IEnumerable<MUsuario>> ConsultarTodos();

    Task<MUsuario> GetById(int id);
}