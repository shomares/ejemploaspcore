﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                 builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
            }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            this.ConfigIoc(services);
            this.ConfigureAuth(services);
            this.ConfigSwagger(services);
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            var key = this.Configuration.GetValue<string>("secret");
         
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

        }

        private void ConfigIoc(IServiceCollection services)
        {

            services.AddDbContext<DulceriaContext>(optionsAction=> optionsAction.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<ITransactional>(s=>s.GetService<DulceriaContext>());
            services.AddTransient<IDaoListaVenta, DaoListaVenta>();
            services.AddTransient<IDaoProducto, DaoProducto>();
            services.AddTransient<IDaoVenta, DaoVenta>();
            services.AddTransient<IDaoBitacoraProducto, DaoBitacoraProducto>();
            services.AddTransient<IDaoUsuario, DaoUsuario>();

            services.AddSingleton<IHashService>(s=> new HashService(10,this.Configuration.GetValue<string>("secret")));
            


            services.AddTransient<IBussinesVenta>(s=>new BussinesVentaTransactional(
                                            new BussinesVenta
                                            (
                                                s.GetService<IDaoProducto>(),
                                                s.GetService<IDaoVenta>(),
                                                s.GetService<IDaoListaVenta>(),
                                                s.GetService<IDaoBitacoraProducto>()
                                            ),
                                            s.GetService<ITransactional>()
                                        ));
            services.AddTransient<IBussinessUsuario>(s=> new BussinesUsuarioTransactional(
                                                        s.GetService<ITransactional>(),
                                                        new BussinesUsuario(
                                                            s.GetService<IDaoUsuario>(),
                                                            s.GetService<IHashService>()
                                                        )
            ));
        }

        private void ConfigSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c=>{
                c.SwaggerDoc("v1", new Info{
                    Version= "v1",
                    Title="Dulceria",
                    Description= "API Dulceria para Quma",
                    TermsOfService= "Free",
                    Contact= new Contact{
                        Name= "Eneas Mejia Shomar",
                        Email= "shomares@gmail.com",
                        Url= string.Empty 
                    }
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("MyPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c=>c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dulceria V1"));
            app.UseAuthentication();
            app.UseMvc();

        }
    }
}
