using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

public interface IHashService
{
    string GetHash(string pass, string salt);
    string GetSalt();

    string GetToken(MUsuario usario);
}

public class HashService : IHashService
{
    private readonly int NumberHash;
    private readonly string Secret;

    public HashService(int NumberHash, string Secret)
    {
        this.NumberHash= NumberHash;
        this.Secret= Secret;
    }

    public string GetHash(string pass, string salt)
    {
        var str = $"{pass}{salt}";

        for(int i=0; i<this.NumberHash; i++)
        {
            str = Sha.GenerateSHA512String(str);
        }

        return str;
    }

    public string GetSalt()
    {
        return Guid.NewGuid().ToString();
    }

    public string GetToken(MUsuario usuario)
    {
          var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(this.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name,usuario.Username )
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
    }
}