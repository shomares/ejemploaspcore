
using System;

public interface ITransactional: IDisposable 
{
    void Commit();

    void Rollback();

    void Begin();

    
}