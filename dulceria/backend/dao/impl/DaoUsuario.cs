using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;  
using System.Linq.Expressions;  
using Microsoft.EntityFrameworkCore;  

public class DaoUsuario : Dao, IDaoUsuario
{
    public DaoUsuario(DulceriaContext Context) : base(Context)
    {
    }

    public async Task<int> Add(Usuario entity)
    {
        await this.Context.Usuario.AddAsync(entity);
        await this.Context.SaveChangesAsync();
        return entity.Id;
    }

    public async Task<bool> Authenticate(string username, string password)
    {
       return await this.Context.Usuario.AnyAsync(s=>s.Username.Equals(username) && password.Equals(password));
    }

    public async Task<IEnumerable<Usuario>> GetAll()
    {
       return await this.Context.Usuario.ToArrayAsync();
    }

    public async Task<Usuario> GetById(int key)
    {
        return await this.Context.Usuario.FindAsync(key);
    }

    public async Task<Usuario> GetByUsername(string username)
    {
       return await this.Context.Usuario.Where(s=>s.Username==username).FirstAsync();
    }

    public async Task<Usuario> Update(Usuario update)
    {
        var user = await this.GetById(update.Id);
        if(user!=null)
        {
            user.ApellidoPaterno= update.ApellidoPaterno;
            user.ApelludoMaterno= update.ApelludoMaterno;
            user.Nombre= update.Nombre;
            user.Password= update.Password;
            await this.Context.SaveChangesAsync();   
        }

        return user;
    }
}