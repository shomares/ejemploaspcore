using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;  
using System.Linq.Expressions;  

public class DaoBitacoraProducto : Dao, IDaoBitacoraProducto
{
    public DaoBitacoraProducto(DulceriaContext Context) : base(Context)
    {
    }

    public async  Task<IEnumerable<MBitacoraProducto>> Consultar(MBitacoraParamatero parametro)
    {
        if(parametro==null)
        {
            parametro= new MBitacoraParamatero{
                FechaFinal= null,
                FechaInicio= null,
                Producto= null,
                ProductoFinal=null
            };
        }

       return await this.Context.BitacoraProducto.FromSql(
                            "select * from ConsultaBitacora({0}, {1}, {2}, {3})",
                            parametro.Producto,
                            parametro.ProductoFinal,
                            parametro.FechaInicio,
                            parametro.FechaFinal
                        )
                    .Include(x=>x.Producto)
                    .Select(x=> new MBitacoraProducto{
                        Fecha= x.Fecha,
                        BitacoraProductoId= x.BitacoraProductoId,
                        Mensaje= x.Mensaje,
                        ProductoId= x.ProductoId,
                        ProductoNombre= x.Producto.Nombre,
                        Tipo= x.Tipo
                    })
            .ToArrayAsync();
    }
}