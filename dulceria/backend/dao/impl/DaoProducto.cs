using System.Collections.Generic;
using System.Threading.Tasks;
using System;  
using System.Linq;  
using System.Linq.Expressions;  
using Microsoft.EntityFrameworkCore;  

public class DaoProducto : Dao, IDaoProducto
{
    public DaoProducto(DulceriaContext context): base(context)
    {

    }
    public async Task<int> Add(Producto entity)
    {
        this.Context.Producto.Add(entity);
        await this.Context.SaveChangesAsync();
        return entity.ProductoId;
    }

    public async Task<IEnumerable<Producto>> GetAll()
    {
       return await this.Context.Producto.ToArrayAsync();
    }

    public async Task<Producto> GetById(int key)
    {
        return await this.Context.Producto.FindAsync(key);
    }

    public async Task<IEnumerable<MVenta>> GetVentasByProducto(int id)
    {
        return await this.Context.Venta
                        .Include(s=>s.ListaVenta)
                        .Where(s=> s.ListaVenta.Any(x=>x.ProductoId==id))
                        .Select(s=>new MVenta
                                {
                                    IdVenta= s.VentaId,
                                    Vendedor= s.Vendedor,
                                    Fecha= s.Fecha,
                                    Ventas= s.ListaVenta.Select(x=> new MListaVenta{
                                        Cantidad= x.Cantidad,
                                        ProductoId= x.ProductoId,
                                        Nombre= x.Producto.Nombre,
                                        VentaId= x.VentaId
                                    })
                        }).ToArrayAsync();
    }

    public async Task<Producto> Update(Producto update)
    {
        var res = await this.GetById(update.ProductoId);
        if(res!=null)
        {
            res.Cantidad = update.Cantidad;
        }
        await this.Context.SaveChangesAsync();
        
        return res;
    }
}