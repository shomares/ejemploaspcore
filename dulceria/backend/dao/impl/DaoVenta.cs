using System.Collections.Generic;
using System.Threading.Tasks;
using System;  
using System.Linq;  
using System.Linq.Expressions;  
using Microsoft.EntityFrameworkCore;  

public class DaoVenta : Dao, IDaoVenta
{
    public DaoVenta(DulceriaContext context): base(context)
    {

    }
    public async Task<int> Add(Venta entity)
    {
        this.Context.Venta.Add(entity);
        await this.Context.SaveChangesAsync();
        return entity.VentaId;
    }

    public async Task<IEnumerable<Venta>> GetAll()
    {
       return await this.Context.Venta.ToArrayAsync();
    }

    public async Task<Venta> GetById(int key)
    {
        return await this.Context.Venta.FindAsync(key);
    }

    public async Task<Venta> Update(Venta update)
    {
        var res = await this.GetById(update.VentaId);
        if(res!=null)
        {
            res.Fecha = update.Fecha;
        }
        await this.Context.SaveChangesAsync();
        
        return res;
    }
}