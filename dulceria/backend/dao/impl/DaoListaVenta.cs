using System.Collections.Generic;
using System.Threading.Tasks;
using System;  
using System.Linq;  
using System.Linq.Expressions;  
using Microsoft.EntityFrameworkCore;  

public class DaoListaVenta : Dao, IDaoListaVenta
{
    public DaoListaVenta(DulceriaContext context): base(context)
    {

    }
    public async Task<int> Add(ListaVenta entity)
    {
        this.Context.ListaVenta.Add(entity);
        await this.Context.SaveChangesAsync();
        return entity.ListaVentaId;
    }

    public async Task<IEnumerable<ListaVenta>> GetAll()
    {
       return await this.Context.ListaVenta.ToArrayAsync();
    }

    public async Task<ListaVenta> GetById(int key)
    {
        return await this.Context.ListaVenta.FindAsync(key);
    }

    public async Task<ListaVenta> Update(ListaVenta update)
    {
        var res = await this.GetById(update.ListaVentaId);
        if(res!=null)
        {
            res.Cantidad = update.Cantidad;
        }
        await this.Context.SaveChangesAsync();
        
        return res;
    }
}