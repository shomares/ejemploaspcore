using System.Collections.Generic;
using System.Threading.Tasks;

public interface IDaoProducto: IDao<Producto, int>
{
    Task<IEnumerable<MVenta>> GetVentasByProducto(int id);
}