using System.Collections.Generic;
using System.Threading.Tasks;

public interface IDaoBitacoraProducto
{
    Task<IEnumerable<MBitacoraProducto>> Consultar (MBitacoraParamatero parametro);
    
}