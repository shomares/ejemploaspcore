using System.Collections.Generic;
using System.Threading.Tasks;

public interface IDao<T,K> 
{
    Task<K> Add(T entity);
    Task<T> Update (T update);

    Task<IEnumerable<T>> GetAll();

    Task<T> GetById(K key);
    
    
}