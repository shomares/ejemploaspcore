using System.Threading.Tasks;

public interface IDaoUsuario : IDao<Usuario, int>
{
    Task<bool> Authenticate(string username, string password);

    Task<Usuario> GetByUsername(string username);
}