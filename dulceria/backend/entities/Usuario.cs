public class Usuario
{
    public int Id {get; set;}
    public string Username {get; set;}

    public string Nombre {get; set;}

    public string ApellidoPaterno {get; set;}

    public string ApelludoMaterno {get; set;}

    public string Password {get; set;}

    public string Salt {get; set;}

    public string Token {get; set;}

    public bool Habilitado {get; set;}

    
}