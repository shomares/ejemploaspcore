using System.ComponentModel.DataAnnotations;

public class ListaVenta
{
    [Key]
    public int ListaVentaId {get; set;}

    public int Cantidad {get; set;}

    public int ProductoId {get; set;}

    public Producto Producto{get; set;}

    public int VentaId {get; set;}

    public Venta Venta {get; set;}
}