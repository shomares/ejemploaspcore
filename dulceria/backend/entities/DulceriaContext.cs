using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

public sealed class  DulceriaContext : DbContext, ITransactional
{
    public DbSet<Producto> Producto{get;set;}

    public DbSet<ListaVenta> ListaVenta{get; set;}

    public DbSet<Venta> Venta {get; set;}

    public DbSet<BitacoraProducto> BitacoraProducto {get ; set;}

    public DbSet <Usuario> Usuario {get; set;}

    private IDbContextTransaction transaction;

 public DulceriaContext(DbContextOptions<DulceriaContext> options)                            : base(options)         
{         
}       
    
    public void Begin()
    {
        this.transaction= this.Database.BeginTransaction();
    }

    public void Commit()
    {
        if(this.transaction!=null)
        {
           this.transaction.Commit();
        }
    }

    public void Rollback()
    {
        if(this.transaction!=null)
        {
           this.transaction.Rollback();
        }
    }
}