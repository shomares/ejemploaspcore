using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

public class Venta
{
    [Key]
    public int VentaId {get; set;}

    public string Vendedor {get; set;}

    public DateTime Fecha {get; set;}

    public virtual ICollection<ListaVenta> ListaVenta {get; set;}
}