using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

public class Producto
{
    [Key]

    public int ProductoId {get; set;}

    public string Nombre {get; set;}

    public int Cantidad {get; set;}

    public DateTime Fecha{get; set;}

    public virtual ICollection<ListaVenta> Ventas{get; set;}
}