using System;
using System.Collections.Generic;

public class MProducto
{
    public int? ProductoId {get; set;}

    public string Nombre {get; set;}

    public int Cantidad {get; set;}

    public DateTime Fecha{get; set;}

    public virtual IEnumerable<MListaVenta> Ventas{get; set;}
}