using System;
using System.ComponentModel.DataAnnotations;

public class MBitacoraProducto
{
    public int BitacoraProductoId {get; set;}

    public string Mensaje  {get; set;}

    public int Tipo {get; set;}

    public DateTime Fecha {get; set;}



    public int ProductoId {get; set;}

    public string ProductoNombre {get; set;}

    public int NumeroVentas {get; set;}
}