using System;
using System.Collections.Generic;

public class MVenta
{
    public int IdVenta {get; set;}

    public string Vendedor {get; set;}
    public DateTime Fecha {get; set;}

    public virtual IEnumerable<MListaVenta> Ventas {get; set;}
}