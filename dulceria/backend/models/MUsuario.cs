public class MUsuario
{
    public int Id {get; set;}
    public string Username {get; set;}

    public string Nombre {get; set;}

    public string ApellidoPaterno {get; set;}

    public string ApelludoMaterno {get; set;}

    public string Password {get; set;}
    public bool Habilitado { get; internal set; }
    public object Token { get; internal set; }
}