public class MListaVenta
{
    public int ListaVentaId {get; set;}

    public int Cantidad {get; set;}

    public int ProductoId {get; set;}

    public string Nombre {get; set;}

    public int VentaId {get; set;}
}