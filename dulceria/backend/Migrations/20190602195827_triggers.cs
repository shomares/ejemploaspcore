﻿using System.IO;
using Microsoft.EntityFrameworkCore.Migrations;

namespace backend.Migrations
{
    public partial class triggers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string sql = $"{Directory.GetCurrentDirectory()}/database/triggersfunction.sql";
            migrationBuilder.Sql(File.ReadAllText(sql));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
