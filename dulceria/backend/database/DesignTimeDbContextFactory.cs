using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DulceriaContext>
{
    public DulceriaContext CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        var builder = new DbContextOptionsBuilder<DulceriaContext>();
        var connectionString = configuration.GetConnectionString("MigrationConnection");
        builder.UseNpgsql(connectionString);
        return new DulceriaContext(builder.Options);
    }
}