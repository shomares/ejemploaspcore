CREATE OR REPLACE FUNCTION auditlogproducto() RETURNS TRIGGER AS $example_table$
   BEGIN
      INSERT INTO public."BitacoraProducto"(Mensaje, Tipo, Fecha, ProductoId) 
      VALUES ("Agregar Producto",1,  current_timestamp, new."ProductoId");
      RETURN NEW;
END;
$example_table$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION auditlogproductoUpdate () RETURNS TRIGGER AS $example_table$
DECLARE 
    texto varchar;
BEGIN
  
    texto:= "Modificar Producto, con cantidad: " || new."Cantidad";
    INSERT INTO public."BitacoraProducto"(Mensaje, Tipo, Fecha, ProductoId) 
    VALUES (texto,2,  current_timestamp, new."ProductoId");
    RETURN NEW;
END;
$example_table$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS triggerProductoNuevo 
    ON public."Producto";


CREATE  TRIGGER triggerProductoNuevo AFTER INSERT
ON public."Producto" FOR EACH ROW 
    EXECUTE PROCEDURE auditlogproducto();


DROP TRIGGER IF EXISTS triggerProductoUpdate 
    ON public."Producto";

CREATE  TRIGGER triggerProductoUpdate AFTER UPDATE
ON public."Producto" FOR EACH ROW 
    EXECUTE PROCEDURE auditlogproductoUpdate();

CREATE OR REPLACE FUNCTION ConsultaBitacora(Producto int default NULL, ProductoFinal INT default NULL, FechaInicio timestamp default NULL,FechaFinal timestamp default NULL )
RETURNS TABLE("BitacoraProductoId" INT,"Mensaje" text, "Tipo" int,"Fecha" timestamp, "ProductoId" int   )
AS $$
BEGIN
    
    RETURN QUERY
            SELECT 
                Bita."BitacoraProductoId", 
                Bita."Mensaje",
                Bita."Tipo",
                Bita."Fecha",
                Bita."ProductoId"
            FROM 
                public."BitacoraProducto" Bita
            WHERE
                (
                    (Bita."ProductoId" >= Producto OR Producto IS NULL) 
                    AND 
                    (Bita."ProductoId" <= ProductoFinal OR ProductoFinal IS NULL)
                )
                AND
                (
                    (Bita."Fecha">=FechaInicio OR FechaInicio IS NULL)
                    AND
                    (Bita."Fecha">=FechaFinal OR FechaFinal IS NULL)
                );
    
END;
$$ LANGUAGE plpgsql;