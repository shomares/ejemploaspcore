using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductoController : ControllerBase
    {

        private readonly IBussinesVenta busssinesVenta;

        /// <summary>
        /// Crea una instancia de ProductoController
        /// </summary>
        /// <param name="busssinesVenta">Implementacion de IBussinesVenta</param> 
        public ProductoController(IBussinesVenta busssinesVenta)
        {
            this.busssinesVenta = busssinesVenta;
        }

        /// <summary>
        /// Obtiene una lista de productos
        /// </summary>
        /// <response code="200">Regresa la lista de productos</response>
        /// <response code="404">Si no hay productos</response>     
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]      
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            var s=  await this.busssinesVenta.GetAll();

            if(s!=null)
            {
                return this.Ok(s);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Consulta un solo producto
        /// </summary>
        /// <param name="id">Id de producto a buscar</param> 
        /// <response code="200">Regresa el producto por id</response>
        /// <response code="404">Si no existe el producto</response>       
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]      
        public async Task<IActionResult> GetVentaByProducto(int id)
        {
            var s = await this.busssinesVenta.GetVentasByProducto(id);

            if(s!=null)
            {
                return this.Ok(s);
            }

            return this.NotFound();
        }
      
        /// <summary>
        /// Suministra un producto
        /// </summary>
        /// <param name="value">Producto a suministrar</param> 
        /// <response code="200">Regresa el producto con id</response>
        /// <response code="404">Si no se realiza el suministro</response>       
        [HttpPost]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]      

        public async Task<IActionResult> Post([FromBody] MProducto value)
        {
            var s = await this.busssinesVenta.Suministrar(value);

            if(s!=null)
            {
                return this.Ok(s);
            }

            return this.NotFound();
        }

        /// <summary>
        /// Consulta bitacora del los productos
        /// </summary>
        /// <param name="value">Parametros de consulta</param> 
        /// <response code="200">ELementos de consulta de bitacora</response>
        /// <response code="404">La consulta no regreso resultados</response>       
        [HttpPost("bitacora")]
        [Authorize]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]      

        public async Task<IActionResult> ConsultaBitacora([FromBody] MBitacoraParamatero value=null)
        {
            var s = await this.busssinesVenta.ConsultarBitacora(value);

            if(s!=null)
            {
                return this.Ok(s);
            }

            return this.NotFound();
        }
    }
}
