using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    /// <summary>
    ///  Venta controller
    /// </summary>
    public class VentaController : ControllerBase
    {

        private readonly IBussinesVenta busssinesVenta;

        /// <summary>
        ///  Crea una instancia de VentaController
        /// </summary>
        /// <param name="busssinesVenta">Instancia de IBussinesVenta</param> 
        public VentaController(IBussinesVenta busssinesVenta)
        {
            this.busssinesVenta = busssinesVenta;
        }

        /// <summary>
        /// Realiza un venta 
        /// </summary>
        /// <param name="value">Venta a registrar</param> 
        /// <response code="200">El resultado de la venta con su id</response>
        /// <response code="404">En caso 404 por que no se registro la venta</response>  
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]  
        [Authorize]
          
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MVenta value)
        {
            var s = await this.busssinesVenta.Vender(value);

            if(s!=null)
            {
                return this.Ok(s);
            }

            return this.NotFound();
        }
    }
}
