using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
[Produces("application/json")]
public class UsuarioController : ControllerBase
{
    private readonly IBussinessUsuario iBussinessUsuario;

    public UsuarioController(IBussinessUsuario iBussinessUsuario)
    {
        this.iBussinessUsuario= iBussinessUsuario;
    }

    /// <summary>
    /// Valida si el usuario tiene acceso a la API
    /// </summary>
    /// <response code="200">EL usuario tiene acceso a la api</response>
    /// <response code="401">El usuario no tiene acceso a la api</response>     
    [HttpPost("authorize")]
     [AllowAnonymous]   
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]   
    public async Task<IActionResult>  Authenticate(MUsuario user)
    {
        var res = await this.iBussinessUsuario.Authenticate(user.Username, user.Password);

        if(res!=null)
        {
            return this.Ok(res);
        }
        else
        {
            return this.Unauthorized();
        }
    }

    /// <summary>
    /// Obtiene un usuario en base al id
    /// </summary>
    /// <response code="200">EL usuario si existe</response>
    /// <response code="404">El usuario no existe</response>     

    [HttpGet("{id}")]
    [Authorize]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]   
   
    public async Task<IActionResult> GetUserById (int id)
    {
        var res  = await this.iBussinessUsuario.GetById(id);

        if(res!=null)
        {
            return this.Ok(res);
        }
        else
        {
            return this.NotFound();
        }
    }

    /// <summary>
    /// Desahibilta a un usuario
    /// </summary>
    /// <response code="200">EL usuario si existe</response>
    /// <response code="404">El usuario no existe</response>     

    [HttpDelete("{id}")]
    [Authorize]
    public async Task<IActionResult> Disabled(int id)
    {
        var res = await this.iBussinessUsuario.Borrar(id);

        if(res==true)
        {
            return this.Ok();
        }
        else
        {
            return this.NotFound();
        }
    }

    /// <summary>
    /// Obtiene a todos los usuarios
    /// </summary>
    /// <response code="200">Lista de usuarios</response>
    /// <response code="404">No hay usuarios registrados</response>     

    [HttpGet]
    [Authorize]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]   
  
    public async Task<IActionResult> GetUsuarios()
    {
        var res = await this.iBussinessUsuario.ConsultarTodos();

        if(res!=null)
        {
            return this.Ok(res);
        }else
        {
            return this.NotFound();
        }
    }

   /// <summary>
    /// Registra a los usuarios
    /// </summary>
    /// <response code="200">Lista de usuarios</response>
    /// <response code="404">No hay usuarios registrados</response>     

    [HttpPost]
    //[Authorize]
    
     [ProducesResponseType(200)]
    [ProducesResponseType(404)]   
  
    public async Task<IActionResult> Registrar(MUsuario usuario)
    {
        var res = await this.iBussinessUsuario.Registrar(usuario);

        if(res!=null)
        {
            return this.Ok();
        }else
        {
            return this.NotFound();
        }
    }

   /// <summary>
    /// Actuaiza a los usuarios
    /// </summary>
    /// <response code="200">Actualiza el usuario</response>
    /// <response code="404">No hay usuarios registrados</response>     

    [HttpPut]
    [Authorize]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]   
  
    public async Task<IActionResult> Update (MUsuario usuario)
    {
        var res = await this.iBussinessUsuario.Editar(usuario);

        if(res!=null)
        {
            return this.Ok();
        }
        else
        {
            return this.NotFound();
        }
    }
}